﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweepwr
{
     class Program : Propertychanged
    {
      

        
        private bool[,] ContainsBomb;

        
        private bool[,] CellClicked;

        
        private bool[,] FlagPlaced;

        public int SizeOfButton { get; private set; } = 30;

        
        public bool GameOver { get; set; }

        
        private int numberOfBombs;

        private Random random;




        public int NumberOfRows { get; set; }



        public int NumberOfColumns { get; set; }




        public int ProbabilityLevel { get; set; }


        private int _numberOfBombs;
        public int NumberOfBombs
        {
            get
            {
                return _numberOfBombs;
            }
            set
            {
                _numberOfBombs = value;
                OnPropertyChanged(nameof(NumberOfBombs));
            }
        }

        private int _numberOfRevealedCells;
        public int NumberOfClickedCells
        {
            get
            {
                return _numberOfRevealedCells;
            }
            set
            {
                _numberOfRevealedCells = value;
                OnPropertyChanged(nameof(NumberOfClickedCells));
            }
        }



        
        public Program(int NumberofRows = 10, int NumberOfColumns = 10, int ProbabilityLevel = 15)
        {
            this.NumberOfRows = NumberofRows;
            this.NumberOfColumns = NumberOfColumns;
            this.ProbabilityLevel = ProbabilityLevel;

            random = new Random();

            NewGame();
        }

        



        
        public bool ChechWin()
        {
            int totalCells = NumberOfColumns * NumberOfRows;
            int notBombCells = totalCells - numberOfBombs;
            if (notBombCells == _numberOfRevealedCells)
            {
                GameOver = true;
                return true;
            }
            else
                return false;
        }

        
        public void NewGame()
        {
            ContainsBomb = new bool[NumberOfRows, NumberOfColumns];
            CellClicked = new bool[NumberOfRows, NumberOfColumns];
            FlagPlaced = new bool[NumberOfRows, NumberOfColumns];
            NumberOfClickedCells = 0;
            numberOfBombs = 0;

            GameOver = false;

            for (int i = 0; i < NumberOfRows; i++)
            {
                for (int j = 0; j < NumberOfColumns; j++)
                {
                    ContainsBomb[i, j] = PlaceMines();
                    if (ContainsBomb[i, j])
                        numberOfBombs++;

                    CellClicked[i, j] = false;
                    FlagPlaced[i, j] = true;
                }
            }

            NumberOfBombs = numberOfBombs;
        }
        public string GetCellContent(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);


            if (!CellClicked[x, y])
            {

                CellClicked[x, y] = true;

                NumberOfClickedCells++;
            }


            return CountMinesNearby(x, y);
        }

        public void SetFlag(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (FlagPlaced[x, y])
            {
                FlagPlaced[x, y] = false;
                NumberOfBombs--;
            }
            else
            {
                FlagPlaced[x, y] = true;
                NumberOfBombs++;
            }



        }

        
        public bool FlagCheck(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (FlagPlaced[x, y])
                return true;
            else
                return false;
        }

        
        public bool ClickedCell(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (CellClicked[x, y])
                return true;
            else
                return false;
        }



        public string GetDificulty(int ProbabilityLevel)
        {
            if (ProbabilityLevel == 5)
                return "Easy";
            else if (ProbabilityLevel == 15)
                return "Medium";
            else
            {
                return "Hard";
            }


        }

        public string Neighbour1(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (x - 1 >= 0 && y - 1 >= 0)
                return string.Format($"X{x - 1}Y{y - 1}");
            else
                return string.Empty;
        }

        
        public string Neighbour2(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (x - 1 >= 0)
                return string.Format($"X{x - 1}Y{y}");
            else
                return string.Empty;
        }

        
        public string Neighbour3(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (x - 1 >= 0 && y + 1 < NumberOfColumns)
                return string.Format($"X{x - 1}Y{y + 1}");
            else
                return string.Empty;
        }

        
        public string Neighbour4(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (y - 1 >= 0)
                return string.Format($"X{x}Y{y - 1}");
            else
                return string.Empty;
        }

        
        public string Neighbour5(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (y + 1 < NumberOfColumns)
                return string.Format($"X{x}Y{y + 1}");
            else
                return string.Empty;
        }

        
        public string Neighbour6(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (x + 1 < NumberOfColumns && y - 1 >= 0)
                return string.Format($"X{x + 1}Y{y - 1}");
            else
                return string.Empty;
        }

        
        public string Neighbour7(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (x + 1 < NumberOfRows)
                return string.Format($"X{x + 1}Y{y}");
            else
                return string.Empty;
        }

        
        public string Neighbour8(string nameOfButton)
        {
            int x = XAxis(nameOfButton);
            int y = YAxis(nameOfButton);

            if (x + 1 < NumberOfRows && y + 1 < NumberOfColumns)
                return string.Format($"X{x + 1}Y{y + 1}");
            else
                return string.Empty;
        }


        private string CountMinesNearby(int x, int y)
        {
            int totalBombs = 0;

            if (ContainsBomb[x, y])
                return "Bomb";

            else
            {
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        if (x + i >= 0 && x + i <= NumberOfRows - 1 && y + j >= 0 && y + j <= NumberOfColumns - 1 && ContainsBomb[x + i, y + j])
                            totalBombs++;
                    }
                }
            }
            return totalBombs.ToString();
        }

        private int XAxis(string nameOfButton)
        {
            string xCoordinates = nameOfButton.Remove(0, 1);
            int indexX = nameOfButton.IndexOf('Y');
            xCoordinates = xCoordinates.Remove(indexX - 1);
            int x = int.Parse(xCoordinates);
            return x;
        }

        
        private int YAxis(string nameOfButton)
        {
            int indexY = nameOfButton.IndexOf('Y');
            string yCoordinates = nameOfButton.Remove(0, indexY + 1);
            int y = int.Parse(yCoordinates);
            return y;
        }

        
        

        
        private bool PlaceMines() 
        {
            int randomNumber = random.Next(100);
            if (randomNumber > ProbabilityLevel)
                return false;
            else
                return true;
        }

        
    }
}
