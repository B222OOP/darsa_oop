﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bomby
{
    internal class Winners
    {
        public string name;
        public string dificulty;
        public string time;
        

        public Winners(string name, string dificulty, string time)
        {
            this.name = name;
            this.dificulty = dificulty;
            this.time = time;
        }

        public override string ToString()
        {
            return $"{name} {time}";
        }
    }
}
