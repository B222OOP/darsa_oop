﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Minesweepwr;
using System.Windows.Threading;
using MySqlConnector;

namespace bomby
{

    public partial class MainWindow : Window
    {
        Program program; 
        DispatcherTimer timer; 
        Stopwatch stopwatch; 
        string CurrentTime = string.Empty; 
        DB_connect dbconn;        
        MySqlDataReader reader;
        bool end = true;



        public MainWindow()
        {

            InitializeComponent();
            MessageBox.Show("Děkuji že hraješ moji hru prosím zadej svoje jméno do políčka místo Hosta\nPokud tak neučiníš tvoje výsledky budou pouze cvičné a nebudou se počítat do tabulky vítězů ");
            program = new Program();
            DataContext = program; 
            

            SetWidthHeight();   

            SetNewPlayground(); 
        }

        private void Leader_Click(object sender, RoutedEventArgs e)
        {
            List<Winners> winners = new List<Winners>();
            dbconn = new DB_connect();
            reader = dbconn.Select("SELECT * from Minesweaper");

            while(reader.Read())
            {
                winners.Add(new Winners(reader.GetString(0), reader.GetString(1), reader.GetString(2)));
            }

              List<Winners> winn = winners.OrderBy(o => o.time).ToList();

            WindowWiners windowWiners = new WindowWiners();
            
            foreach (var win in winn)
            {
                if(win.dificulty == "Easy")
                {
                    
                    windowWiners.Easy.Items.Add(win.name + " " + win.time);
                }
                
                if (win.dificulty == "Medium")
                {
                    windowWiners.Medium.Items.Add(win.name + " " + win.time);
                }
                    
                if (win.dificulty == "Hard")
                {
                    windowWiners.Hard.Items.Add(win.name + " " + win.time);
                }
            }
            

            windowWiners.ShowDialog();

        }



       
        private void SetNewPlayground()
        {
            
            PlaygroundGrid.Children.Clear(); 
            PlaygroundGrid.RowDefinitions.Clear();
            PlaygroundGrid.ColumnDefinitions.Clear();

            
            for (int i = 0; i < program.NumberOfColumns; i++)
                PlaygroundGrid.ColumnDefinitions.Add(new ColumnDefinition());

            
            for (int i = 0; i < program.NumberOfRows; i++)
                PlaygroundGrid.RowDefinitions.Add(new RowDefinition());


            
            for (int i = 0; i < program.NumberOfRows; i++)
            {
                for (int j = 0; j < program.NumberOfColumns; j++)
                {
                    Button btn = new Button();
                    btn.Name = string.Format($"X{i}Y{j}");
                    btn.Background = Brushes.Green;
                    btn.Click += Left_Click;
                    btn.MouseRightButtonDown += Right_Click;
                    Grid.SetRow(btn, i);
                    Grid.SetColumn(btn, j);
                    PlaygroundGrid.Children.Add(btn);
                }
            }

            
            timer = new DispatcherTimer();
            stopwatch = new Stopwatch();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
            stopwatch.Start();
        }

        
        



       
        private void Left_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            
            if (program.FlagCheck(btn.Name) && !program.GameOver && !program.ClickedCell(btn.Name))
            {
                string numberOfMines = program.GetCellContent(btn.Name);
                btn.FontWeight = FontWeights.ExtraBold;
                btn.Background = Brushes.White;
                btn.BorderBrush = Brushes.Gray;
                if (numberOfMines == "1")
                    btn.Foreground = Brushes.Blue;
                if (numberOfMines == "2")
                    btn.Foreground = Brushes.DarkGreen;
                if (numberOfMines == "3")
                    btn.Foreground = Brushes.Red;
                if (numberOfMines == "4")
                    btn.Foreground = Brushes.Purple;
                if (numberOfMines == "5")
                    btn.Foreground = Brushes.Navy;
                if (numberOfMines == "6")
                    btn.Foreground = Brushes.LimeGreen;
                if (numberOfMines == "7")
                    btn.Foreground = Brushes.Black;
                if (numberOfMines == "8")
                {
                    btn.Foreground = Brushes.White;
                    btn.Background = Brushes.Black;
                }

                if (numberOfMines == "Bomb")
                {
                    
                    Image img = new Image();
                    string y = System.AppDomain.CurrentDomain.BaseDirectory;                    
                    string x = "bomb.png";

                    string path = y + x;
                    img.Source = new BitmapImage(new Uri(path)); 

                    btn.Content = img;
                    btn.Background = Brushes.White;
                    program.GameOver = true;
                    

                    
                    stopwatch.Stop();

                    
                    ShowGameOver();
                    return;
                }
                else
                {
                    if (numberOfMines == "0")
                    {
                        RevealeNearCell(btn.Name);
                        btn.Content = string.Empty;
                    }

                    else
                        btn.Content = program.GetCellContent(btn.Name);
                }

               
                
            }

            if (program.ChechWin() && end)
            {
                
                stopwatch.Stop();
                end = false;

                
                ShowWin();
                return;
            }

        }
        private void RevealeNearCell(string nameOfButton)
        {
            string Neighbour1 = program.Neighbour1(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour1))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour1).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

            string Neighbour2 = program.Neighbour2(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour2))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour2).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

            string Neighbour3 = program.Neighbour3(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour3))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour3).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

            string Neighbour4 = program.Neighbour4(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour4))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour4).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

            string Neighbour5 = program.Neighbour5(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour5))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour5).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

            string Neighbour6 = program.Neighbour6(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour6))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour6).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

            string Neighbour7 = program.Neighbour7(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour7))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour7).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

            string Neighbour8 = program.Neighbour8(nameOfButton);
            if (!string.IsNullOrEmpty(Neighbour8))
            {
                Button btn = PlaygroundGrid.Children.OfType<Button>().Where(t => t.Name == Neighbour8).FirstOrDefault();
                btn.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }

        }

        private void Right_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (!program.ClickedCell(btn.Name) && !program.GameOver)
            {

                if (program.FlagCheck(btn.Name))
                {
                    Image img = new Image();
                    string y = System.AppDomain.CurrentDomain.BaseDirectory;
                    string x = "flag.png";

                    string path = y + x;
                    img.Source = new BitmapImage(new Uri(path));
                    btn.Content = img;
                    btn.Background = Brushes.White;
                    program.SetFlag(btn.Name);

                }
                else
                {
                    btn.Content = string.Empty;
                    btn.Background = Brushes.Green;
                    program.SetFlag(btn.Name);
                }


            }

        }

        
        

        
        void timer_Tick(object sender, EventArgs e)
        {
            TimeSpan ts = stopwatch.Elapsed;
            CurrentTime = String.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);

        }



        private void SetWidthHeight()
        {
            int suposedWidth = program.NumberOfColumns * program.SizeOfButton;
            if (suposedWidth < 400)
            {
                PlayGroundColumn.Width = new GridLength(suposedWidth, GridUnitType.Pixel);
                this.Width = 400;

            }
            else
            {
                PlayGroundColumn.Width = new GridLength(suposedWidth - 40, GridUnitType.Pixel);
                this.Width = program.NumberOfColumns * program.SizeOfButton;

            }
            this.Height = program.NumberOfRows * program.SizeOfButton + 80;
        }



        private void ShowGameOver()
        {
            MessageBox.Show($"Prohrál jsi.\nZkus lehčí obtížnost a příště to snad vyjde.");
        }

        
        private void ShowWin()
        {

            dbconn = new DB_connect();
            MessageBox.Show($"Vyhrál jsi, Tvůj čas byl: {CurrentTime}");
            if(Player.Text != "Host")
            {
                string insert = $"INSERT INTO Minesweaper(Name,Difuculty,Time) VALUES ('{Player.Text}','{program.GetDificulty(program.ProbabilityLevel)}','{CurrentTime}')";

                dbconn.Insert(insert);
            }

            

        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            SetWidthHeight();
            end = true;
            SetNewPlayground();
            program.NewGame();
            while (program.NumberOfBombs == 0)
            {
                program.NewGame();
            }
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {


            if (Procento.SelectedValue.ToString() == "Easy")
            {
                program.NumberOfRows = 5;
                program.NumberOfColumns = 5;
                program.ProbabilityLevel = 5;
            }

            else if (Procento.SelectedValue.ToString() == "Medium")
            {
                program.NumberOfRows = 10;
                program.NumberOfColumns = 10;
                program.ProbabilityLevel = 15;
            }
            else
            {
                program.NumberOfRows = 15;
                program.NumberOfColumns = 15;
                program.ProbabilityLevel = 25;
            }


            SetWidthHeight();
            SetNewPlayground();
            program.NewGame();
        }

        
    }
}
