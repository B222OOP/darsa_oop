﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace bomby
{
    /// <summary>
    /// Interaction logic for WindowWiners.xaml
    /// </summary>
    public partial class WindowWiners : Window
    {
        public WindowWiners()
        {
            InitializeComponent();
        }



        private void Close_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
