﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv_06_console
{
    internal class Zvire
    {
        public string jmeno;
        public int vek;
        public int pocet_noh;
        public virtual string barva { get; set; }

        public Zvire(string jmeno, int vek, int pocet_noh, string barva)
        {
            this.jmeno = jmeno;
            this.vek = vek;
            this.pocet_noh = pocet_noh;
            this.barva = barva;
        }

        public virtual string Rev()
        {
            return "Takhle rve zvire";
        }

        public string BArva()
        
            {
            return barva;
            }


        public string GetName()
        {
            return $"{jmeno}";
        }
        private string GetLegs()
        {
            return $"{pocet_noh}";
        }
        protected string GetVek()
        {
            return $"{vek}";
        }

    }
}
