﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cv_06_console
{
    internal class Pes:Zvire
    {
        public string breed;
        public override string barva { get => base.barva + "Změna"; set => base.barva = value; }

        public Pes(string jmeno, int vek, int pocet_noh,string breed,string barva): base(jmeno, vek, pocet_noh,barva)
        {
            this.breed = breed;           
        }

        public void TryMethod()
        {
            Console.WriteLine(GetVek());
        }

        public override string Rev()
        {
            return "Takhle štěká pes";
        }
    }
}
