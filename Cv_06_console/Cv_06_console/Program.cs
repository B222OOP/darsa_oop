﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv_06_console
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Pes pes = new Pes("Max", 8, 4, "Malamut","Černý");
            Zvire zvire = new Zvire("Borec", 666, 3, "Zlatá");
            Console.WriteLine(pes.Rev());
            Console.WriteLine(pes.GetName());
            pes.TryMethod();
            Console.WriteLine(pes.BArva());
            Console.WriteLine();
            Console.WriteLine(zvire.Rev());
            Console.WriteLine(zvire.GetName());
            Console.WriteLine(zvire.BArva());

            Console.ReadKey();
        }
    }
}
