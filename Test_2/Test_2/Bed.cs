﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_2
{
    public class Bed
    {
        Patient patient;
        public bool volno = true;
        public int nostnost;
        public string jmeno;

        public Bed(int nostnost, string jmeno)
        {
            this.nostnost = nostnost;
            this.jmeno = jmeno;
        }

        public void AddPatient(Patient patient)
        {
            this.patient = patient;
            volno = false;
        }

        public void RemmovePatient(Patient patient)
        {
            volno = true;
            this.patient = patient;

        }

        public string BedInfo()
        {
            if (volno == true)
            {
                return "Postel je prázdná";
            }
            else
                return patient.jmeno;
        }


        public override string ToString()
        {
            return $"{jmeno}";
        }



    }
}
