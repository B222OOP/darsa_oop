﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_2
{
    public class Patient
    {
        public string jmeno;
        public string prijmeni;
        public double rc;
        public int vaha;
        public int vyska;

        public Patient(string jmeno, string prijmeni, double rc, int vaha, int vyska) //Hodnoty jsem nechal předvolené aby se nemuseli pokaždé pracnš zadívat a řešit problém s padáním programu dokud tam nejsou podmínky
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.rc = rc;
            this.vaha = vaha;
            this.vyska = vyska;
        }


        public override string ToString()
        {
            return $"{jmeno} "; // Potom Pridat prijmeni {Prijmeni}
        }
    }
}
