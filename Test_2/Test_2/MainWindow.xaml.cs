﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //TODO: Nejak to zkulturni
        //TODO:Dodělat veškeré chybové hlášení
        //TODO: Podívat se pořádně proč nefunguej procházení zpětně pokojů
        //TODO:Odebrani pacienta z pokoje

        public static List<Bed> Bedlist = new List<Bed>();
        public static List<Patient> patientList = new List<Patient>();
        public static List<Room> roomlist = new List<Room>();
        int i = 1;
        int j = 1;
        public int Index;
        Patient vybrany_Patient;
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void Pridej_Pac_btn_Click(object sender, RoutedEventArgs e)
        {
            AddPatient addPatient = new AddPatient();
            addPatient.ShowDialog();
            Patient patient = new Patient(addPatient.Jmeno_pac.Text, addPatient.Prijmeni_pac.Text,double.Parse(addPatient.RC_pac.Text), Int32.Parse(addPatient.Vaha_pac.Text), Int32.Parse(addPatient.Vyska_pac.Text));
            patientList.Add(patient);
            Kartoteka.Items.Add(patient);
        }

        private void Vytvor_pokoj_btn_Click(object sender, RoutedEventArgs e)
        {
            AddRoom addRoom = new AddRoom();
            addRoom.ShowDialog();
            roomlist.Add(new Room(int.Parse(addRoom.Max_Nostnost.Text), int.Parse(addRoom.Poc_Posteli.Text)));
            ListBox_Pokoje.Items.Add($"Pokoj {i}");
            i++;
        }

        private void ListBox_Pokoje_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            ListBox_Postele.Visibility = Visibility.Visible;
            int index = ListBox_Pokoje.SelectedIndex;
            ListBox_Postele.Items.Clear(); //Musím mazat jinak se mi ty postele akorát načítají dál a dál dyštak zkusit vymyslet jiné řešení
            
            roomlist[index].inicializacePosteli();
             
            foreach(var p in roomlist[index].VypsaniPOosteli())
            {
                ListBox_Postele.Items.Add(p);
                Bedlist.Add(p);
            }                 
        }

        private void Kartoteka_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            vybrany_Patient = (Patient)Kartoteka.SelectedItem;
            
        }



        private void ListBox_Postele_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {



            Index = ListBox_Postele.SelectedIndex;
            Info_pokoj.Content = Bedlist[Index].BedInfo();//Nemuzu se vrátit zpátky na pokoje z nějakého důvodu mně to vzdycky vyhodí Index -1
        }

        private void Pacient_Na_Pokoj_Click(object sender, RoutedEventArgs e)
        {
            if ((int)Bedlist[Index].nostnost > (int)vybrany_Patient.vaha)
            {
                if (Bedlist[Index].volno)
                {
                    Bedlist[Index].AddPatient(vybrany_Patient);
                    patientList.Remove(vybrany_Patient);
                    Kartoteka.Items.Clear();
                    foreach (var p in patientList)
                    {
                        Kartoteka.Items.Add(p);
                    }
                }
                else
                    MessageBox.Show("Postel je obsazena");
            }
            else
                MessageBox.Show("Postel ¨nemá dostatešnou nosnost");


        }

        //private void Odebrani_Pacienta_Click(object sender, RoutedEventArgs e)
        //{
        //    if (!Bedlist[Index].volno)
        //    {
        //        Bedlist[Index].RemmovePatient(Bedlist[Index]);
                
        //    }
        //}
    }
}
