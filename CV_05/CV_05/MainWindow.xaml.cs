﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CV_05
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Point position;
        Random random = new Random();
        int height;
        int width;
        Rectangle rct;
        DispatcherTimer timer;
        private bool smer = true;

        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(dispechertimer_Tick);
            timer.Interval = TimeSpan.FromMilliseconds(10);
            
        }

        private void dispechertimer_Tick(object sender, EventArgs eventArgs)
        {



            if(this.position.X<this.width-41&&smer)
            {
                this.position.X += 5;
                this.position.Y += 5;
                Canvas.SetLeft(this.rct, this.position.X);
                //Canvas.SetTop(this.rct, this.position.Y);
            }
            else
            {
                this.position.X -= 5;
                this.position.Y -= 5;
                Canvas.SetLeft(this.rct, this.position.X);
                smer = false;
                if (this.position.X < 0)
                    smer = true;
                //Canvas.SetTop(this.rct, this.position.Y);
            }

        }

        public void AddRectangle()
        {

            for (int x = 0; x < 20; x++)
            {
                
                rct = new Rectangle();
                rct.Fill = new SolidColorBrush(Colors.Blue);
                rct.Width = 40;
                rct.Height = 20;

                height = (int)cnv.Height;
                width = (int)cnv.Width;

                this.position = new Point();
                this.position.X = random.Next(width-40);//-40 abychom měli pouze na canvasu
                this.position.Y = random.Next(height-20);

                

                Canvas.SetLeft(rct, this.position.X);
                Canvas.SetTop(rct, this.position.Y);


                cnv.Children.Add(rct);

            }


        }

        private void tlacido_Click(object sender, RoutedEventArgs e)
        {
            AddRectangle();
            timer.Start();
        }
    }
}
