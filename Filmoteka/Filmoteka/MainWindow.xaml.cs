﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySqlConnector;

namespace Filmoteka
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    //TODO:Podle času dodělat čas u rezervace
    public partial class MainWindow : Window
    {
        
        MySqlDataReader reader;
        DB_connect dbconn;
        User user;
        int id;

        public MainWindow()
        {
            Begin:
            dbconn = new DB_connect();
            LoginWindow login = new LoginWindow();
            login.ShowDialog();
            string name = login.Name.Text;
            string pasword = login.Pasword.Text;

            reader = dbconn.Select($"SELECT * FROM Uzivatel1 " +
                    $"WHERE Name = \"{name}\" AND Pasword = \"{pasword}\"");

            while (reader.Read())
            {
                id = reader.GetInt32(0);
                user = new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
                    
            }
            dbconn.CloseConn();

            if (user == null)
            {
                MessageBox.Show("Jmeno nebo heslo se neshoduje");
                goto Begin;

            }

            else if (user.IRole == 1)
            {
                user.Role = Role.Admin.ToString();
                AdminWindow admin = new AdminWindow();
                admin.LabelNick.Content = $"{user.NickName} (Admin)";
                admin.ShowDialog();
                


            }

            else if(user.IRole == 2)
            {
                user.Role = Role.Zakaznik.ToString();
                ZakaznikWindow zakaznik = new ZakaznikWindow();
                zakaznik.LabelNick.Content = $"{user.NickName} (Customer) ";
                zakaznik.GEtID(id);
                
                zakaznik.ShowDialog();
    
            }

            
            Close(); //zavřená mainwindow
        }
        
    }
}
