﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySqlConnector;

namespace Filmoteka
{
    /// <summary>
    /// Interaction logic for ZakaznikWindow.xaml
    /// </summary>
    public partial class ZakaznikWindow : Window
    {
        MySqlDataReader reader;
        DB_connect dbconn;
        User user;
        Movie movie;
        
        public int id { get; set; }
        int Movie_ID;

        public void GEtID(int IF)
        {
            id = IF;
        }
        public ZakaznikWindow()
        {
            
            InitializeComponent();
            dbconn = new DB_connect();

            #region VacantFilms
            List<Movie> movietList = new List<Movie>();
            dbconn = new DB_connect();

            reader = dbconn.Select($"SELECT * FROM Movie WHERE Rezervation = 0");
            while (reader.Read())
            {

                movietList.Add(new Movie(reader.GetString(1), reader.GetString(2),
                reader.GetBoolean(3)));
            }
            dbconn.CloseConn();



            foreach (var o in movietList)
            {
                vacantFilm.Items.Add(o.Name);
            }
            #endregion


            List<int> movieID = new List<int>();
            List<string> movie = new List<string>();


            reader = dbconn.Select($"SELECT * FROM Rezervation WHERE Uzivatel1_ID  = {id}");
            while (reader.Read())
            {
                
                movieID.Add(reader.GetInt32(2));
            }
            dbconn.CloseConn();

            
            foreach(var o in movieID)
            {
                reader = dbconn.Select($"SELECT * FROM Movie WHERE ID_Movie = 9");
                while (reader.Read())
                {

                    movie.Add(reader.GetString(1));
                }
                dbconn.CloseConn();
            }
            
            foreach (var o in movie)
            {
                RezerverdFilm.Items.Add(o);
            }


        }
        

        private void Profile_Settings_Click(object sender, RoutedEventArgs e)
        {
            

            reader = dbconn.Select($"SELECT * FROM Uzivatel1 WHERE ID_User = \"{id}\"");
            while (reader.Read())
            {
                
                user = new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
            }
            dbconn.CloseConn();

            UpdateCustumer updateCustumer = new UpdateCustumer();
            updateCustumer.Name.Text = user.Name;
            updateCustumer.Surname.Text = user.Surname;
            updateCustumer.NickName.Text = user.NickName;
            updateCustumer.Pasword.Text = user.Pasword;
            updateCustumer.Role.Text = user.IRole.ToString();
            updateCustumer.ShowDialog();
            dbconn.Update(String.Format($"UPDATE Uzivatel1 SET Name = \"{updateCustumer.Name.Text}\", Surname = \"{updateCustumer.Surname.Text}\",Pasword = \"{updateCustumer.Pasword.Text}\", NickName = \"{updateCustumer.NickName.Text}\", Role_ID = \"{int.Parse(updateCustumer.Role.Text)}\"  WHERE ID_User=\"{id}\""));

        }


    }
}
