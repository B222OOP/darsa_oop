﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using MySqlConnector;

namespace Filmoteka
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        MySqlDataReader reader;
        DB_connect dbconn;
        User user;
        Movie movie;
        
        int? id;
        int Uzivatel1_ID;
        int Movie_ID;


        public AdminWindow()
        {
            InitializeComponent();

            #region Zakazníci // Načítaní Zde se načítají zakazníci
            List<User> patientList = new List<User>();
            dbconn = new DB_connect();

            reader = dbconn.Select($"SELECT * FROM Uzivatel1 " +
                    $"WHERE Role_ID = \"2\"");
            while (reader.Read())
            {

                patientList.Add(new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }
            dbconn.CloseConn();



            foreach (var o in patientList)
            {
                lstcos.Items.Add(o.Name + " " + o.Surname);
            }
            #endregion

            #region Free Movies
            List<Movie> movietList = new List<Movie>();
            dbconn = new DB_connect();

            reader = dbconn.Select($"SELECT * FROM Movie WHERE Rezervation = 0");
            while (reader.Read())
            {

                movietList.Add(new Movie(reader.GetString(1), reader.GetString(2),
                reader.GetBoolean(3)));
            }
            dbconn.CloseConn();



            foreach (var o in movietList)
            {
                FreeMovies.Items.Add(o.Name);
            }



            #endregion

            #region Rezervace
            List<Rezervation> rezervationList = new List<Rezervation>();
            dbconn = new DB_connect();

            reader = dbconn.Select($"SELECT * FROM Rezervation ");
            while (reader.Read())
            {
                Uzivatel1_ID = reader.GetInt32(1);
                Movie_ID = reader.GetInt32(2);
            }
            dbconn.CloseConn();

            reader = dbconn.Select($"SELECT * FROM Uzivatel1 " +
                    $"WHERE ID_User = \"{Uzivatel1_ID}\"");
            while (reader.Read())
            {

                user = new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
            }
            dbconn.CloseConn();

            reader = dbconn.Select($"SELECT * FROM Movie WHERE ID_Movie = \"{Movie_ID}\"");
            while (reader.Read())
            {

                movie = new Movie(reader.GetString(1), reader.GetString(2),
                reader.GetBoolean(3));
            }
            dbconn.CloseConn();



            rezervationList.Add(new Rezervation(user, movie));



            foreach (var o in rezervationList)
            {
                Rezervation.Items.Add(o.Movie.Name);
            }



            #endregion
        }

        #region Costumer // Všechny funkce k zákazníkovy

        private void Inspect_Click(object sender, RoutedEventArgs e)
        {


            string vybrano = lstcos.SelectedItem.ToString();
            string[] vabranoSplit = vybrano.Split(' ');

            reader = dbconn.Select($"SELECT * FROM `Uzivatel1` WHERE Name = \"{vabranoSplit[0]}\" AND Surname = \"{vabranoSplit[1]}\"");
            while (reader.Read())
            {
                id = reader.GetInt32(0);
                user = new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
            }


            MessageBox.Show($"{user.Name} {user.Surname} {user.Pasword} {user.NickName}");

            
            reader = dbconn.Select($"SELECT * FROM `Rezervation` WHERE Uzivatel1_ID = {id}");
            id = null;
            while (reader.Read())
            {
                
                id = reader.GetInt32(2);
            }
            dbconn.CloseConn();

            if (id != null)
            {
                reader = dbconn.Select($"SELECT * FROM `Movie` WHERE ID_Movie = {id}");
                while (reader.Read())
                {
                    movie = new Movie(reader.GetString(1), reader.GetString(2), reader.GetBoolean(3));
                }
                dbconn.CloseConn();


            
                MessageBox.Show($"{movie.Name} {movie.Genre}");
            }

        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {

            CeateCostumer customur = new CeateCostumer();
            customur.ShowDialog();
            string Name = customur.Name.Text;
            string Surname = customur.Surname.Text;
            string Pasword = customur.Pasword.Text;
            string NickName = customur.NickName.Text;
            int Role = int.Parse(customur.Role.Text);

            string insert = $"INSERT INTO Uzivatel1(Name,Surname,Pasword,NickName,Role_ID) VALUES (\"{Name}\",\"{Surname}\"," +
                $"\"{Pasword}\",\"{NickName}\", {Role})";

            dbconn.Insert(insert);

            List<User> patientList = new List<User>();
            dbconn = new DB_connect();

            reader = dbconn.Select($"SELECT * FROM Uzivatel1 " +
                    $"WHERE Role_ID = \"2\"");
            while (reader.Read())
            {

                patientList.Add(new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }
            dbconn.CloseConn();

            lstcos.Items.Clear();

            foreach (var o in patientList)
            {
                lstcos.Items.Add(o.Name + " " + o.Surname);
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            List<User> userList = new List<User>();
            dbconn = new DB_connect();
            string vybrano = lstcos.SelectedItem.ToString();
            string[] vabranoSplit = vybrano.Split(' ');
            dbconn.Delete($"DELETE FROM Uzivatel1 WHERE Name = \"{vabranoSplit[0]}\" AND Surname = \"{vabranoSplit[1]}\"");

            lstcos.Items.Clear();

            reader = dbconn.Select("SELECT * from Uzivatel1");
            while (reader.Read())
            {

                userList.Add(new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }
            dbconn.CloseConn();



            foreach (var o in userList)
            {
                lstcos.Items.Add(o.Name + " " + o.Surname);
            }

        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            string vybrano = lstcos.SelectedItem.ToString();
            string[] vabranoSplit = vybrano.Split(' ');
            reader = dbconn.Select($"SELECT * FROM Uzivatel1 WHERE Name = \"{vabranoSplit[0]}\" AND Surname = \"{vabranoSplit[1]}\"");
            while (reader.Read())
            {
                id = reader.GetInt32(0);
                user = new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
            }
            dbconn.CloseConn();

            UpdateCustumer updateCustumer = new UpdateCustumer();
            updateCustumer.Name.Text = user.Name;
            updateCustumer.Surname.Text = user.Surname;
            updateCustumer.NickName.Text = user.NickName;
            updateCustumer.Pasword.Text = user.Pasword;
            updateCustumer.Role.Text = user.IRole.ToString();
            updateCustumer.ShowDialog();
            dbconn.Update(String.Format($"UPDATE Uzivatel1 SET Name = \"{updateCustumer.Name.Text}\", Surname = \"{updateCustumer.Surname.Text}\",Pasword = \"{updateCustumer.Pasword.Text}\", NickName = \"{updateCustumer.NickName.Text}\", Role_ID = \"{int.Parse(updateCustumer.Role.Text)}\"  WHERE ID_User=\"{id}\""));


            List<User> userList = new List<User>();

            lstcos.Items.Clear();

            reader = dbconn.Select("SELECT * from Uzivatel1 WHERE Role_ID = 2 ");
            while (reader.Read())
            {

                userList.Add(new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }
            dbconn.CloseConn();



            foreach (var o in userList)
            {
                lstcos.Items.Add(o.Name + " " + o.Surname);
            }





        }

        #endregion

        #region Movie

        private void CreateMovie_Click(object sender, RoutedEventArgs e)
        {
            Createmovie createmovie = new Createmovie();
            createmovie.ShowDialog();
            string Name = createmovie.Name.Text;
            string Genre = createmovie.Genre.Text;
            

            string insert = $"INSERT INTO Movie(Name,Genre,Rezervation) VALUES (\"{Name}\",\"{Genre}\",0)";
            dbconn.Insert(insert);

            List<Movie> MovietList = new List<Movie>();
            dbconn = new DB_connect();

            reader = dbconn.Select($"SELECT * FROM Movie " +
                    $"WHERE Rezervation = 0");
            while (reader.Read())
            {
                MovietList.Add(new Movie(reader.GetString(1), reader.GetString(2),
                reader.GetBoolean(3)));
            }
            dbconn.CloseConn();

            FreeMovies.Items.Clear();

            foreach (var o in MovietList)
            {
                FreeMovies.Items.Add(o.Name);
            }

        }

        private void DeleteMovie_Click(object sender, RoutedEventArgs e)
        {
            List<Movie> movieList = new List<Movie>();
            dbconn = new DB_connect();
            string vybrano = FreeMovies.SelectedItem.ToString();
            
            dbconn.Delete($"DELETE FROM Movie WHERE Name = \"{vybrano}\"");

            FreeMovies.Items.Clear();

            reader = dbconn.Select("SELECT * from Movie WHERE Rezervation = 0");
            while (reader.Read())
            {

                movieList.Add(new Movie(reader.GetString(1), reader.GetString(2),
                reader.GetBoolean(3)));
            }
            dbconn.CloseConn();



            foreach (var o in movieList)
            {
                FreeMovies.Items.Add(o.Name);
            }
        }
        private void InspectMovie_Click(object sender, RoutedEventArgs e)
        {
            List<Movie> movieList = new List<Movie>();
            dbconn = new DB_connect();
            string vybrano = FreeMovies.SelectedItem.ToString();
            reader = dbconn.Select($"SELECT * from Movie WHERE Name = \"{vybrano}\"");
            while (reader.Read())
            {

                movie = new Movie(reader.GetString(1), reader.GetString(2),
                reader.GetBoolean(3));
            }
            dbconn.CloseConn();

            MessageBox.Show($"{movie.Name} {movie.Genre}");

        }

        #endregion

        #region Rezervation
        private void InspectRez_Click(object sender, RoutedEventArgs e)
        {
            string vybrano = Rezervation.SelectedItem.ToString();

            reader = dbconn.Select($"SELECT * FROM `Movie` WHERE Name = \"{vybrano}\"");
            while (reader.Read())
            {
                Movie_ID = reader.GetInt32(0);
                movie = new Movie(reader.GetString(1), reader.GetString(2), reader.GetBoolean(3));
            }
            dbconn.CloseConn();

            reader = dbconn.Select($"SELECT * FROM `Rezervation` WHERE Movie_ID = \"{Movie_ID}\"");
            while (reader.Read())
            {
                
                Uzivatel1_ID = reader.GetInt32(1);
            }
            dbconn.CloseConn();

            reader = dbconn.Select($"SELECT * FROM `Uzivatel1` WHERE ID_User = \"{Uzivatel1_ID}\"");
            while (reader.Read())
            {                
                user = new User(reader.GetString(1), reader.GetString(2),
                reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
            }
            dbconn.CloseConn();
           
            MessageBox.Show($"{user.Name} {user.Surname} {user.Pasword} {user.NickName}\n{movie.Name} {movie.Genre}");            
        }

        #endregion

        
    }
}
    

