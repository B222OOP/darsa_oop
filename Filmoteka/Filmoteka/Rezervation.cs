﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filmoteka
{
    public class Rezervation
    {
        public User User { get; set; }
        public Movie Movie { get; set; }
        //public DateTime Date { get; set; }

        public Rezervation(User user, Movie movie )
        {
            User = user;
            Movie = movie;
            //Date = date;
        }
    }
}
