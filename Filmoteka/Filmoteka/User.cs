﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;

namespace Filmoteka
{
    public class User
    {
        
        public string Name { get; set; }
        public string Surname { get; set; }
        
        public string Pasword { get; set; }
        public string NickName { get; set; }
        public int IRole { get; set; } 
        public string Role { get; set; }

        public User( string name, string surname, string pasword, string nickName, int irole)
        {
            NickName = nickName;
            Name = name;
            Surname = surname;            
            Pasword = pasword;
            IRole = irole;
        }

    }
}
