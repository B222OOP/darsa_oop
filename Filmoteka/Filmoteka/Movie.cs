﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filmoteka
{
    public class Movie
    {
        public string Name { get; set; }
        public string Genre{ get; set; }
        public bool Rezervation { get; set; }//kdyby nefungovalo tak to dát jako 1 a 0

        public Movie(string name, string genre, bool rezervation)
        {
            Name = name;
            Genre = genre;
            Rezervation = rezervation;
        }
    }
}
