﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_database
{
    public class Patient
    {
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public int BirthNumber { get; private set; }
        public int Insurance { get; private set; }

        public Patient(string name, string surname, int birthNumber, int insurance)
        {
            this.Name = name;
            this.Surname = surname;
            this.BirthNumber = birthNumber;
            this.Insurance = insurance;
        }

        public string toString()
        {
            return String.Format("Pacient:{0} {1} ma rodne cislo:{2} a pojistovnu:{3}", this.Name, this.Surname, this.BirthNumber, this.Insurance);
        }
    }
}
