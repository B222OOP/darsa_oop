﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySqlConnector;
using static System.Net.Mime.MediaTypeNames;

namespace WPF_database
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        DB_connect dbconn;
        public Patient pat;
        MySqlDataReader reader;
        public MainWindow()
        {
            InitializeComponent();

            List<Patient> patientList = new List<Patient>();
            dbconn = new DB_connect();

            reader = dbconn.Select("SELECT * from Pacient"); 
            while (reader.Read())
            {

                patientList.Add(new Patient(reader.GetString(1), reader.GetString(2), reader.GetInt32(3),
                    reader.GetInt32(4)));
            }
            dbconn.CloseConn();



            foreach (var o in patientList)
            {
                lstbx.Items.Add(o.Name + " " + o.Surname);
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            AddPatient addPatient = new AddPatient();
            addPatient.ShowDialog();


            string insert = $"INSERT INTO Pacient(Name,Surname,BirthNumber,Insurence) VALUES ('{addPatient.Name.Text}','{addPatient.Surname.Text}',{addPatient.BirthNumber.Text},{addPatient.Insurense.Text})";
            dbconn.Insert(insert);

            List<Patient> patientList = new List<Patient>();
            lstbx.Items.Clear();
            reader = dbconn.Select("SELECT * from Pacient"); 
            while (reader.Read())
            {
                
                patientList.Add(new Patient(reader.GetString(1), reader.GetString(2), reader.GetInt32(3),
                    reader.GetInt32(4)));
            }
            dbconn.CloseConn();


            
            foreach (var o in patientList)
            {
                lstbx.Items.Add(o.Name + " " + o.Surname);
            }



        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            List<Patient> patientList = new List<Patient>();
            dbconn = new DB_connect();
            string vybrano = lstbx.SelectedItem.ToString();
            string[] vabranoSplit = vybrano.Split(" ");
            dbconn.Delete($"DELETE FROM Pacient WHERE Name = '{vabranoSplit[0]}';");

            lstbx.Items.Clear();

            reader = dbconn.Select("SELECT * from Pacient"); 
            while (reader.Read())
            {

                patientList.Add(new Patient(reader.GetString(1), reader.GetString(2), reader.GetInt32(3),
                    reader.GetInt32(4)));
            }
            dbconn.CloseConn();



            foreach (var o in patientList)
            {
                lstbx.Items.Add(o.Name + " " + o.Surname);
            }

        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            

            EditPatient editPatient = new EditPatient();
            
            string vybrano = lstbx.SelectedItem.ToString();
            string[] vabranoSplit = vybrano.Split(" ");

            reader = dbconn.Select($"SELECT * FROM `Pacient` WHERE Name = '{vabranoSplit[0]}'");
            while(reader.Read())
            {
                pat = new Patient(reader.GetString(1), reader.GetString(2), reader.GetInt32(3), reader.GetInt32(4));
            }
            dbconn.CloseConn();

            editPatient.Name.Text = pat.Name;
            editPatient.Surname.Text = pat.Surname;
            editPatient.BirthNumber.Text = pat.BirthNumber.ToString();
            editPatient.Insurense.Text = pat.Insurance.ToString();

            dbconn.Delete($"DELETE FROM Pacient WHERE Name = '{vabranoSplit[0]}';");//je řešeno takto neojrabaně za boha jsem nomohl rozchodit update
            dbconn.CloseConn();
            editPatient.ShowDialog();
            lstbx.Items.Clear();

            string insert = $"INSERT INTO Pacient(Name,Surname,BirthNumber,Insurence) VALUES ('{editPatient.Name.Text}','{editPatient.Surname.Text}',{editPatient.BirthNumber.Text},{editPatient.Insurense.Text})";
            dbconn.Insert(insert);

            dbconn.CloseConn();

            List<Patient> patientList = new List<Patient>();
            lstbx.Items.Clear();
            reader = dbconn.Select("SELECT * from Pacient");
            while (reader.Read())
            {

                patientList.Add(new Patient(reader.GetString(1), reader.GetString(2), reader.GetInt32(3),
                    reader.GetInt32(4)));
            }
            dbconn.CloseConn();



            foreach (var o in patientList)
            {
                lstbx.Items.Add(o.Name + " " + o.Surname);
            }

        }
    }
}
