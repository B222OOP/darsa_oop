﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CV_06
{
    public class Dog
    {
        public string jmeno;
        public string breed;
        public int age;

        public Dog(string jmeno, string breed, int age)
        {
            this.jmeno = jmeno;
            this.breed = breed;
            this.age = age;
        }
    }
}
