﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CV_06
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static List<Dog> dogList = new List<Dog>();


        public MainWindow()
        {
            InitializeComponent();
        }

        public void btn_load_Click(object sender, RoutedEventArgs e)
        {
            foreach(var dog in dogList)
            {
                ListBox.Items.Add(dog.jmeno);
            }
            List<string> listStingu = new List<string>(new string[] { "Jedna", "Dva", "tri", "ctyri", "Pet", "Sest", "Sedm", "Osm", "Devet", "10" });
            foreach (var o in listStingu)
            {
                ListBox.Items.Add(o);
            }


        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            //ListBox.Items.Remove(ListBox.SelectedItem);
            while (ListBox.SelectedItems.Count > 0)
            {
                ListBox.Items.Remove(ListBox.SelectedItem);
            }
        }

        private void pridat_Click(object sender, RoutedEventArgs e)
        {
           ListBox.Items.Add(textbox.Text);
            
        }

        public void btn_Click(object sender, RoutedEventArgs e)
        {
            okno2 okno2 = new okno2();
            okno2.ShowDialog();

            dogList.Add(new Dog(okno2.jmeno.Text, okno2.plemeno.Text, int.Parse(okno2.vek.Text)));
        }
        
    }
}
