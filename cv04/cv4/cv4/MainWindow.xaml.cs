﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cv4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        int a = 1;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
        //    lbl.Content = "Ahoj světe";
        //    if(lbl.Content.ToString() == "Ahoj světe")
        //    {
        //        lbl.Content = "HEllo wolrd";
        //    }
        //    else
        //    {
        //        lbl.Content = "Ahoj světe";
        //    }
                
            //lbl.Content  += texbox.Text + ", ";
            //lbl.Content.ToString();

            //int pomocna = 1;
            //pomocna = a + pomocna;

            //texbox.Text = $"{pomocna}";
            //a++;
        }
        private void scitani_Click(object sender, RoutedEventArgs e)
        {
            textblock.Text = (int.Parse(textblock.Text) + int.Parse(textbox.Text)).ToString();
            textbox.Text = "";
        }

        private void odcitani_Click(object sender, RoutedEventArgs e)
        {
            textblock.Text = (int.Parse(textblock.Text) - int.Parse(textbox.Text)).ToString();
            textbox.Text = "";
        }

        private void nasobeni_Click(object sender, RoutedEventArgs e)
        {
            textblock.Text = (int.Parse(textblock.Text) * int.Parse(textbox.Text)).ToString();
            textbox.Text = "";

        }

        private void deleni_Click(object sender, RoutedEventArgs e)
        {
            textblock.Text = (int.Parse(textblock.Text) / int.Parse(textbox.Text)).ToString();
            textbox.Text = "";

        }

        private void modulo_Click(object sender, RoutedEventArgs e)
        {
            textblock.Text = (int.Parse(textblock.Text) % int.Parse(textbox.Text)).ToString();
            textbox.Text = "";
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            textblock.Text = 0.ToString();
        }
    }
}
