﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Cv02_new
{
    internal class Bed
    {

        Patient patient;
        bool volno = true;
        
        public void VlozeniPacienta(Patient patient)
        {
            if(volno)
            {
                this.patient = patient;
                volno = false;
                patient.PacientemPO(true);
            }
        }

        public void PropusteniPacienta(Patient patient)
        {
            this.patient.PacientemPO(false);
            this.patient = null;
            volno = true;
            
        }

        public bool Volno()
        {
            return volno;
        }

        public void PacientInfo()
        {
            Console.WriteLine(patient);
        }






    }
}
