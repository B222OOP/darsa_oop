﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace Cv02_new
{
    internal class Room
    {
        int nosnost;
        int pocLuzek;
        int pocPac = 0;
        int poradi;
        int misto;


        List<Bed> bed = new List<Bed>();





        public Room(int poradi, int nosnost, int pocLuzek)
        {
            this.nosnost = nosnost;
            this.pocLuzek = pocLuzek;
            this.poradi = poradi;


            for (int i = 0; i < pocLuzek; i++)
            {
                Bed b = new Bed();
                bed.Add(b);
            }


        }

        public void PridejPac(Patient patient, int misto)
        {
            
            if (patient.Vahapatienta() > nosnost)
            {
                Console.WriteLine($"Bohužel váha pacienta {patient} přesahuje nostnost zkuste jiný pokoj");
                
            }


            else if (!bed[misto - 1].Volno())
            {
                Console.WriteLine("Bohužel pokoj je obsazen");
            }


            else if (bed[misto - 1].Volno())
            {
                bed[misto - 1].VlozeniPacienta(patient);
                pocPac++;
                Console.WriteLine($"{patient} byl umístěn na lůžko {misto} v pokoji {poradi}");
                Console.WriteLine($"Pocet volnych mist na pokoji {poradi}. je {pocLuzek - pocPac} postel");
            }


        }


        public void OdeberPac(Patient patient, int misto)
        {
            
            if (!bed[misto - 1].Volno())
            {
                bed[misto - 1].PropusteniPacienta(patient);
                Console.WriteLine($"{patient} byl propuštěn z pokoje {poradi} a uvolnilo se lůžko {misto}");
                pocPac--;
                Console.WriteLine($"Pocet volnych mist na pokoji {poradi}. je {pocLuzek - pocPac} postel");
            }
        }

        public void PacientinaPokoji()
        {
            int postel=1;
            foreach(Bed b in bed)
            {   
                Console.Write($"Postel číslo {postel}: ");
                b.PacientInfo();
                postel++;   
                
            }
        }


        







    }
}
