﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv02_new
{
    internal class Patient
    {
        string jmeno;
        string prijmeni;
        int rodCislo;
        int vaha;
        int vyska;
        bool pacientem;
        double bmi;


        public Patient(string jmeno, string prijmeni, int rodCislo, int vaha, int vyska, bool pacientem)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.rodCislo = rodCislo;
            this.vaha = vaha;
            this.vyska = vyska;
            this.pacientem = pacientem;

            
        }

        public void Nabrani(int nabrani)
        {
            vaha += nabrani;
        }

        public void Zhubnuti(int zhubnuti)
        {
            vaha -= zhubnuti;
        }

        public int Vahapatienta()
        {
            return vaha;
        }

        public double BMI()
        {
            this.bmi = (double)this.vaha / (((double)vyska * (double)vyska) / 10000);
            return bmi = Math.Round(bmi, 2);
        }

        public bool Pacientem()
        {
            return pacientem;
        }

        public bool PacientemPO(bool pacientem)
        {
            if (pacientem)
            {
                return this.pacientem = true;
            }
            else return this.pacientem = false;
            
        }

       
        


        public override string ToString()
        {
            return $"{jmeno} {prijmeni} vaha:{vaha} bmi:{BMI()}";
        }


    }
}
