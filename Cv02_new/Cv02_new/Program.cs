﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv02_new
{

    internal class Program
    {


        static void Main(string[] args)
        {
            int j;
            int i;
            int x;
            bool pokracovat = true;

            List<Patient> pacienti = new List<Patient>();
            Patient pacient1 = new Patient("Pepa", "Hromádka", 1, 50, 190,false);
            Patient pacient2 = new Patient("Jarda", "Hnátek", 1, 70, 190,false);
            Patient pacient3 = new Patient("Prokop", "Dveře", 1, 120, 190,false);
            Patient pacient4 = new Patient("Ferda", "Šiška", 1, 160, 190,false);

            pacienti.Add(pacient1);
            pacienti.Add(pacient2);
            pacienti.Add(pacient3);
            pacienti.Add(pacient4);

            List<Room> mistnosti = new List<Room>();
            Room mistnost1 = new Room(1, 100, 2);
            Room mistnost2 = new Room(2, 150, 3);
            Room mistnost3 = new Room(3, 200, 2);

            mistnosti.Add(mistnost1);
            mistnosti.Add(mistnost2);
            mistnosti.Add(mistnost3);
            mistnost2.PridejPac(pacient1, 2);

            while (pokracovat)
            {
                Console.Clear();
                Console.WriteLine("Vítej ve zdravotnickém informačním systému Darsa");
                Console.WriteLine("Co za akci si přeješ vykonat?");
                Console.WriteLine("1. Vypsat pacientykteří čekají na umístění na pokoj");
                Console.WriteLine("2. Vypsat pacienty na určitém pokoji");
                Console.WriteLine("3. Přiřadit pacienta na pokoj");
                Console.WriteLine("4. Odstranit pacienta z pokoje");
                Console.WriteLine("5. Hubnutí pacienta");
                Console.WriteLine("6. Nabírání pacienta");
                Console.WriteLine("7. Help");
                Console.WriteLine("8. Ukončit program");
                int volba = int.Parse(Console.ReadLine());



                switch (volba)
                {
                    

                    case 1:
                        {
                            foreach (Patient p in pacienti)
                            {
                                if (!p.Pacientem())
                                {
                                    Console.WriteLine(p);
                                }
                            }
                            break;
                        }


                    case 2:
                        {
                            int p = 1;

                            foreach (Room r in mistnosti)
                            {
                                Console.Write($"Pokoj cislo{p}: ");//vypisuje to dobře pacienta na správném místě 
                                r.PacientinaPokoji();
                                p++;
                            }
                            break;
                        }


                    case 3:
                        {

                            Console.Write("Jakého pacienta si přeješ umístit? "); //psát jejich pořadí v lsitu 
                            j = int.Parse(Console.ReadLine());
                            Console.Write("Na jaký pokoj ho chceš umístit? ");
                            i = int.Parse(Console.ReadLine());
                            Console.Write("Na jakou postel ho chceš umístit? ");
                            x = int.Parse(Console.ReadLine());

                            mistnosti[i - 1].PridejPac(pacienti[j - 1], x);
                            break;
                        }


                    case 4:
                        {
                            Console.Write("Jakého pacienta si přeješ propustit? "); //psát jejich pořadí v lsitu 
                            j = int.Parse(Console.ReadLine());
                            Console.Write("Na jakém pokoji se nachází? ");
                            i = int.Parse(Console.ReadLine());
                            Console.Write("Na jaké posteli je? ");
                            x = int.Parse(Console.ReadLine());

                            mistnosti[i - 1].OdeberPac(pacienti[j - 1], x);

                            break;
                        }

                    case 5:
                        {
                            Console.Write("Jaký pacient zhubnul: ");
                            j = int.Parse(Console.ReadLine());
                            Console.Write("O kolik kilo zhubnul: ");
                            i = int.Parse(Console.ReadLine());

                            pacienti[j - 1].Zhubnuti(i);
                            break;
                        }
                    case 6:
                        {
                            Console.Write("Jaký pacient nabral: ");
                            j = int.Parse(Console.ReadLine());
                            Console.Write("O kolik kilo nabral: ");
                            i = int.Parse(Console.ReadLine());

                            pacienti[j - 1].Nabrani(i);
                            break;
                        }

                    case 7:
                        {
                            int z = 1;
                            foreach (Patient p in pacienti)
                            {
                                Console.Write($"Pacient {z} je: ");
                                Console.WriteLine(p);
                                z++;
                            }
                            break;
                        }


                    case 8:
                        {
                            Console.WriteLine("Konec");
                            pokracovat = false;
                            break;
                        }


                    default:
                        {
                            Console.WriteLine("Nemám možnosti");
                            break;
                        }

                   


                
                }
                Console.ReadKey();
            }
            Console.ReadKey();
        }
    }
}
